#!/bin/bash
export EASYRSA_BATCH=1

OUTPUT_FOLDER=$1
CLIENT_NAME=$2

LOCATION="./"

# Edit the folder accordingly
module="EasyRSA-3.0.8"

$LOCATION/$module/easyrsa build-client-full $CLIENT_NAME nopass
cp pki/issued/$CLIENT_NAME.crt $OUTPUT_FOLDER
cp pki/private/$CLIENT_NAME.key $OUTPUT_FOLDER
