#!/bin/bash
export EASYRSA_BATCH=1

OUTPUT_FOLDER=$1
LOCATION="./"

# Edit the folder accordingly
module="EasyRSA-3.0.8"
$LOCATION/$module/easyrsa init-pki
$LOCATION/$module/easyrsa build-ca nopass
$LOCATION/$module/easyrsa build-server-full server nopass

/bin/mkdir -p $OUTPUT_FOLDER
cp pki/ca.crt $OUTPUT_FOLDER
cp pki/issued/server.crt $OUTPUT_FOLDER
cp pki/private/server.key $OUTPUT_FOLDER
