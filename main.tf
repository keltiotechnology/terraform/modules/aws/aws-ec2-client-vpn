/*
 
 * //////////////////////////////////////////////////////////////////////////////
 *                             Manage Amazon VPN Client
 * ////////////////////////////////////////////////////////////////////////////// 
*/

resource "aws_acm_certificate" "vpn_server_certificate" {
  private_key       = var.server_private_key
  certificate_body  = var.server_certificate_body
  certificate_chain = var.certificate_chain
  tags              = var.tags
}

resource "aws_ec2_client_vpn_endpoint" "vpn" {
  description            = var.vpn_endpoint_description
  client_cidr_block      = var.vpn_cidr_block
  split_tunnel           = var.vpn_split_tunnel
  server_certificate_arn = aws_acm_certificate.vpn_server_certificate.arn

  dns_servers = var.dns_servers

  authentication_options {
    type                       = "certificate-authentication"
    root_certificate_chain_arn = aws_acm_certificate.vpn_server_certificate.arn
  }

  connection_log_options {
    enabled = false
  }

  tags = var.tags
}

resource "aws_security_group" "vpn_access" {
  vpc_id = var.vpc_id
  name   = var.vpn_security_group_name

  ingress {
    from_port   = 443
    protocol    = "UDP"
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow VPN incoming traffic from anywhere"
  }

  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow VPN outgoing traffic to anywhere"
  }

  tags = var.tags
}

resource "aws_ec2_client_vpn_authorization_rule" "vpn_auth_rule" {
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.vpn.id
  target_network_cidr    = var.vpc_cidr_block
  authorize_all_groups   = true
}

resource "aws_ec2_client_vpn_network_association" "vpn_subnets" {
  count = var.enable_aws_ec2_client_vpn_network_association ? length(var.subnets) : 0

  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.vpn.id

  subnet_id       = var.subnets[count.index]
  security_groups = [aws_security_group.vpn_access.id]
  lifecycle {
    # Bug with terraform! 
    # See: https://github.com/hashicorp/terraform-provider-aws/issues/14717
    ignore_changes = [subnet_id]
  }

  depends_on = [
    aws_ec2_client_vpn_authorization_rule.vpn_auth_rule
  ]

}

resource "aws_security_group_rule" "vpn_rules" {
  count = length(var.vpn_security_group_rules)

  type                     = var.vpn_security_group_rules[count.index].type
  from_port                = var.vpn_security_group_rules[count.index].from_port
  to_port                  = var.vpn_security_group_rules[count.index].to_port
  protocol                 = var.vpn_security_group_rules[count.index].protocol
  source_security_group_id = aws_security_group.vpn_access.id
  security_group_id        = var.vpn_security_group_rules[count.index].security_group_id
  description              = var.vpn_security_group_rules[count.index].description
}
