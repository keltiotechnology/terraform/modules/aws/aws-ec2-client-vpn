# AWS EC2 Client VPN

## Description

This module resources to use with AWS Client VPN.
Along the module, we use a tool so-called easy-rsa to manage private certificate authority and certificates.
A set of handy scripts inside folder utils are provided to help working with the tool.

## Requirements

Before using the module, we use the tool easy-rsa to create private authority, key and certificates for server and client(s).

Steps:

1. Go to Release page of easy-rsa: https://github.com/OpenVPN/easy-rsa/releases
2. Choose the suitable build for your machine. For example, if you uses Linux or MacOS, select latest release e.g. "EasyRSA-3.0.8.tgz" to download.
3. Unzip the file

Example using the command line with Linux:

```bash
tar -xvzf EasyRSA-3.0.8.tgz
```

Assuming you extract the content to the folder "EasyRSA-3.0.8", you can use the following scripts in utils folder.

First, allow executable permission for the script. 

Note: In case the folder name is not "EasyRSA-3.0.8", kindly edit the scripts to have correct module name.

```bash
chmod +x pki_generate_ca_server_cert.sh
chmod +x pki_generate_ca_client_cert.sh
```

To create private CA and generate server certificate:

```bash
./pki_generate_ca_server_cert.sh <name of folder to store all certificates and keys>

# Example:
# ./pki_generate_ca_server_cert.sh ca
```

To issue certificate for an user:

```bash
./pki_generate_ca_client <name of folder to store all certificates and keys> <name of user>

# Example:
#./pki_generate_ca_client_cert.sh ca peter
```

And that now you have necessary files to inside the specified folder which can be used to allow mutual authentication with Amazon Client VPN.

Note:

- The folder pki is automatically generated by the tool easy-rsa.
- The specified folder (in the examples, I called "ca") is just for store and distribute certificates easier.
- You should keep these scripts and generated files outside of terraform project.

## Usage

### Using this module
```hcl-terraform
module "client_vpn" {
  source                    = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-ec2-client-vpn"

  # Provide the path to the files
  certificate_chain         = "${abspath(path.root)}/my_ca/ca.crt"
  server_private_key        = "${abspath(path.root)}/my_ca/server.key"
  server_certificate_body   = "${abspath(path.root)}/my_ca/server.crt"

  vpc_cidr_block            = var.vpc_cidr_block
  vpc_id                    = module.vpc.vpc_id

  vpn_cidr_block            = "10.0.20.0/22"    
  dns_servers               = ["172.16.0.2"] # VPC CIDR Base Network + 2
  subnets                   = module.vpc.private_subnet_ids
  
  vpn_security_group_rules  = [
    {
      "type"                            = "ingress"
      "from_port"                       = 8
      "to_port"                         = 0
      "protocol"                        = "ICMP"
      "security_group_id"               = module.ecs_cluster.vpc_default_security_group_id
    },
    {
      "type"                            = "ingress"
      "from_port"                       = 8000
      "to_port"                         = 8000
      "protocol"                        = "TCP"
      "security_group_id"               = module.ecs_cluster.vpc_default_security_group_id
    }
  ]
  
  vpn_security_group_name   = "<client>-vpn-secgroup"
}
```

Note:

- The dns_servers is a list of string where we specify the DNS server IP addresses. You can specify the VPC CIDR Base Network plus 2.
For example, the CIDR address of VPC is "172.16.0.0/16". So the IP "172.16.16.0.0" is the network address. Hence we know that "172.16.0.2" is the reserved IP address of Amazon DNS server. Reference is at [here](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Subnets.html)

### Connect VPN after apply the module


1) After apply the terraform code, you need to visit Amazon Console > VPC > Client VPN Endpoints.
2) Select the created Client VPN endpoint
3) Click the button "Download Client Configuration"
4) Edit the downloaded file with your text editor.
5) You will see in the first lines something like this

```bash
remote cvpn-endpoint-<RANDOM NUMBER>.prod.clientvpn.<AWS-REGION>.amazonaws.com 443
```

6) You need to append the hostname with a random string e.g. "abc" into the hostname like this

```bash
remote abc.cvpn-endpoint-<RANDOM NUMBER>.prod.clientvpn.<AWS-REGION>.amazonaws.com 443
```

After that, you can distribute the openvpn file along with the generated client certificate and client key to your users.
For more guides, please visit this [page](https://docs.aws.amazon.com/vpn/latest/clientvpn-user/connect.html).

Note that after you connect VPN, you can only access to resources inside VPN network (given all security rules/policies are setup).

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.56.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_acm_certificate.vpn_server_certificate](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/acm_certificate) | resource |
| [aws_ec2_client_vpn_authorization_rule.vpn_auth_rule](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_client_vpn_authorization_rule) | resource |
| [aws_ec2_client_vpn_endpoint.vpn](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_client_vpn_endpoint) | resource |
| [aws_ec2_client_vpn_network_association.vpn_subnets](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ec2_client_vpn_network_association) | resource |
| [aws_security_group.vpn_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group_rule.vpn_rules](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_certificate_chain"></a> [certificate\_chain](#input\_certificate\_chain) | Certificate chain of server certificate | `string` | n/a | yes |
| <a name="input_dns_servers"></a> [dns\_servers](#input\_dns\_servers) | DNS server(s) used to resolve DNS. We can use Amazon DNS server which is the base of the VPC IPv4 network range plus two | `list(string)` | n/a | yes |
| <a name="input_server_cert_tag"></a> [server\_cert\_tag](#input\_server\_cert\_tag) | A tag for the server certificate tag | `string` | `"VPN server certificate"` | no |
| <a name="input_server_certificate_body"></a> [server\_certificate\_body](#input\_server\_certificate\_body) | Server certificate body | `string` | n/a | yes |
| <a name="input_server_private_key"></a> [server\_private\_key](#input\_server\_private\_key) | Server private key | `string` | n/a | yes |
| <a name="input_subnets"></a> [subnets](#input\_subnets) | A list of subnets to associate with the Amazon Client VPN endpoint | `list(string)` | n/a | yes |
| <a name="input_vpc_cidr_block"></a> [vpc\_cidr\_block](#input\_vpc\_cidr\_block) | The target network CIDR | `string` | n/a | yes |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | VPC ID | `string` | n/a | yes |
| <a name="input_vpn_cidr_block"></a> [vpn\_cidr\_block](#input\_vpn\_cidr\_block) | VPN CIDR block | `string` | n/a | yes |
| <a name="input_vpn_client_vpn_endpoint_tag"></a> [vpn\_client\_vpn\_endpoint\_tag](#input\_vpn\_client\_vpn\_endpoint\_tag) | A tag for the VPN Client VPN Endpoint | `string` | `"Client VPN Endpoint"` | no |
| <a name="input_vpn_endpoint_description"></a> [vpn\_endpoint\_description](#input\_vpn\_endpoint\_description) | The vpn endpoint description | `string` | `"Client VPN"` | no |
| <a name="input_vpn_security_group_name"></a> [vpn\_security\_group\_name](#input\_vpn\_security\_group\_name) | Name of the security group for the Client VPN Endpoint | `string` | `"swappy-vpn-secgroup"` | no |
| <a name="input_vpn_security_group_rules"></a> [vpn\_security\_group\_rules](#input\_vpn\_security\_group\_rules) | List of security group rules to add for the vpn | <pre>list(object({<br>    type              = string<br>    from_port         = number<br>    to_port           = number<br>    protocol          = string<br>    security_group_id = string<br>  }))</pre> | n/a | yes |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
