/*
 * Server certificate variables
*/
variable "certificate_chain" {
  type        = string
  description = "Certificate chain of server certificate"
}

variable "server_private_key" {
  type        = string
  description = "Server private key"
}

variable "server_certificate_body" {
  type        = string
  description = "Server certificate body"
}

variable "server_cert_tag" {
  type        = string
  description = "(Deprecated) A tag for the server certificate tag"
  default     = "VPN server certificate"
}

variable "vpn_endpoint_description" {
  type        = string
  description = "The vpn endpoint description"
  default     = "Client VPN"
}

variable "vpn_cidr_block" {
  type        = string
  description = "VPN CIDR block"
}

variable "vpn_split_tunnel" {
  type        = bool
  description = "Enable or disblae VPN Split tunnel"
  default     = false
}

variable "vpn_client_vpn_endpoint_tag" {
  type        = string
  description = "(Deprecated) A tag for the VPN Client VPN Endpoint"
  default     = "Client VPN Endpoint"
}

variable "subnets" {
  type        = list(string)
  description = "A list of subnets to associate with the Amazon Client VPN endpoint"
}

variable "enable_aws_ec2_client_vpn_network_association" {
  type        = bool
  description = "Enable the Client VPN network association"
  default     = true
}


/*
 * Security group variables
*/
variable "vpn_security_group_name" {
  type        = string
  description = "Name of the security group for the Client VPN Endpoint"
  default     = "swappy-vpn-secgroup"
}

variable "vpc_id" {
  type        = string
  description = "VPC ID"
}

variable "vpc_cidr_block" {
  type        = string
  description = "The target network CIDR"
}

# Detail: https://docs.aws.amazon.com/vpc/latest/userguide/vpc-dns.html
variable "dns_servers" {
  type        = list(string)
  description = "DNS server(s) used to resolve DNS. We can use Amazon DNS server which is the base of the VPC IPv4 network range plus two"
}

variable "vpn_security_group_rules" {
  type = list(object({
    type              = string
    from_port         = number
    to_port           = number
    protocol          = string
    security_group_id = string
    description       = string
  }))
  description = "List of security group rules to add for the vpn"
}

variable "tags" {
  type    = map(string)
  default = {}
}
