output "ec2_client_vpn_endpoint_vpn_id" {
  value = aws_ec2_client_vpn_endpoint.vpn.id
}

output "aws_security_group_vpn_access_id" {
  value = aws_security_group.vpn_access.id
}


